
class Scratch {
    public static void main(String[] args) {
        int[] q={7,3,5,1};
        int[] a=solution(3, q);
        for (int i=0;i<a.length;i++)
            System.out.print(a[i]+" ");
    }
    public static int[] solution(int h, int[] q) {

        int head = (int)Math.pow(2,h) - 1;
        int[] result = new int[q.length];
        for (int i=0; i<q.length;i++){
            if (q[i]<head && q[i]>0)
                result[i]=find(q[i],head,head-1);
            else
                result[i]=-1;
        }
        return result;
    }
    public static int find(int head, int cur, int diff) {
        int right_node=cur-1;
        int left_node=right_node-diff/2;

        if(right_node==head || left_node==head)
            return cur;
        else if(head<=left_node)
            return find(head,left_node,diff/2);
        else
            return find(head,right_node,diff/2);

    }

}
